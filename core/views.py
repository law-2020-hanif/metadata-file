from django.shortcuts import render
import os, requests, json, socket
from django.http import HttpResponse, FileResponse, JsonResponse
from .models import Metadata
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers import serialize
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@csrf_exempt
def controller(request, file_id):
    if isAuthorizationHeaderOkay(request.headers) is False:
            return JsonResponse(jsonErrorMessage('silakan isi Authorization pada header dengan value: Bearer [token auth]'))
    try:
        metadata_object = Metadata.objects.get(id=file_id)
        if isAuthorizedToSeeMetadata(request.headers['Authorization'].split()[1], metadata_object.oauth_client_id) is False:
            return JsonResponse(jsonErrorMessage('Anda tidak memiliki otorisasi untuk mengakses file tersebut'))
        if request.method == 'GET':
            return JsonResponse(metadata_object.as_dict(), safe=False)
        elif request.method == 'POST':
            if 'delete' in request.POST:
                metadata_object.delete()
                return JsonResponse({'message':'deletion was successful'})
            for (request_key, request_value) in request.POST.items():
                setattr(metadata_object, request_key, request_value)
            metadata_object.save()
            return JsonResponse(metadata_object.as_dict(), safe=False)
    except ObjectDoesNotExist:
        return JsonResponse(jsonErrorMessage('file tidak ditemukan'))
        

def seeAllMetas(request):
    if isAuthorizationHeaderOkay(request.headers) is False:
        return JsonResponse(jsonErrorMessage('silakan isi Authorization pada header dengan value: Bearer [token auth]'))
    
    acquired_resources = ambil_resource(request.headers['Authorization'].split()[1])
    if 'error' in acquired_resources:
        return JsonResponse(acquired_resources, safe=False)
    
    meta_data_objects = Metadata.objects.filter(oauth_client_id = acquired_resources['client_id'])
    meta_datas_list = []
    for meta_data_object in meta_data_objects:
        meta_datas_list.append(meta_data_object.as_dict())
    return JsonResponse(meta_datas_list, safe=False)



def isAuthorizedToSeeMetadata(access_token, oauth_client_id_file):
    acquired_resources = ambil_resource(access_token)
    if 'error' in acquired_resources:
        return False
    if acquired_resources['client_id'] == oauth_client_id_file:
        return True
    return False

def isAuthorizationHeaderOkay(request_headers):
    if ('Authorization' not in request_headers) or (request_headers['Authorization'] is None) or (request_headers['Authorization'].split()[1] is None):
        return False
    return True


def ambil_token(user, pasw, client_id, client_secret):
    URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/token"
    payload = {'username': user, 'password': pasw, 'grant_type': 'password', 'client_id': client_id, 'client_secret': client_secret}
    r = requests.post(url = URL, data=payload)
    return json.loads(r.text)

def ambil_resource(access_token):
    URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
    data_headers = { 'Authorization': 'Bearer ' + access_token }
    r = requests.get(url = URL, headers = data_headers)
    return json.loads(r.text)

def jsonErrorMessage(msg):
    return {'error': msg}

@csrf_exempt
def createMeta(request):
    if request.method == 'POST':
        if isAuthorizationHeaderOkay(request.headers) is False:
            return JsonResponse(jsonErrorMessage('silakan isi Authorization pada header dengan value: Bearer [token auth]'))
        params = areParametersCompleted(request.POST, ['file_size', 'downloadable_url', 'file_name'])
        if (len(params) != 0):
            message = generateIncompleteParametersMessage(params)
            return JsonResponse(jsonErrorMessage(message))

        acquired_resources = ambil_resource(request.headers['Authorization'].split()[1])
        if 'error' in acquired_resources:
            return JsonResponse(acquired_resources)
        metadata_object = Metadata.objects.create(oauth_client_id=acquired_resources['client_id'], file_size=request.POST['file_size'],downloadable_url=request.POST['downloadable_url'], file_name=request.POST['file_name'])
        return JsonResponse(metadata_object.as_dict(), safe=False)
    return JsonResponse(jsonErrorMessage('untuk dapat membuat metadata, gunakan method POST'))


def areParametersCompleted(request_body, required_fields):
    incomplete_params = []
    for required_field in required_fields:
        if required_field not in request_body:
            incomplete_params.append(required_field)
    return incomplete_params

def generateIncompleteParametersMessage(list_of_incomplete_params):
    string_response = 'parameter yang dibutuhkan untuk POST tidak lengkap. Parameter yang kurang : '
    for incomplete_param in list_of_incomplete_params:
        string_response = string_response + incomplete_param + ', '
    return string_response[:-2]
