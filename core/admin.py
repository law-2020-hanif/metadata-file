from django.contrib import admin
from .models import Metadata

# Register your models here.

class MetadataAdmin(admin.ModelAdmin):
    pass
admin.site.register(Metadata, MetadataAdmin)
