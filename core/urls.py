from django.urls import path

from . import views

urlpatterns = [
    path('of/<str:file_id>', views.controller, name='controller'),
    path('all', views.seeAllMetas, name='seeAllMetas'),
    path('new', views.createMeta, name='createMeta')
]
