from django.db import models
from django.utils import timezone

# Create your models here.
class Metadata(models.Model):
    oauth_client_id = models.CharField(max_length=100)
    file_size = models.CharField(max_length=100)
    downloadable_url = models.CharField(max_length=500)
    file_name = models.CharField(max_length=500)
    time_saved = models.DateTimeField(auto_now_add=True)

    def as_dict(self):
        return {
            'id':self.id,
            'oauth_client_id': self.oauth_client_id,
            'file_size': self.file_size,
            'downloadable_url': self.downloadable_url,
            'file_name': self.file_name
        }
